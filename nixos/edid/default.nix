{ stdenv, lib, ... }:

stdenv.mkDerivation rec {
  pname = "g9-edid";
  version = "1";

  src = ./firmware;
  installPhase = ''
    mkdir -p $out/lib/firmware
    cp -r * $out/lib/firmware
  '';

  meta = {
    description = "EDID file for Samsung Odyssey G9 Monitor";
    maintainers = [ "Dan Ronning <talasian@gmail.com>" ];
    platforms = lib.platforms.linux;
  };
}
